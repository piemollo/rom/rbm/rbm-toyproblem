// ====== ====== Header
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|   Set up algebraic structures  |" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;

// ------ Include
verbosity = 0;
include "getARGV.idp"
include "./module/ffmatlib/ffmatlib.idp"

// ====== ====== Setting
// ------ Data path
string meshid = getARGV("-mesh","./mesh/Th.msh");
string ooipath= getARGV("-out_ooi","");

// ------ Load mesh	
mesh Th  = readmesh(meshid);
fespace Vh(Th,P2);
fespace Qh(Th,P1);

// --- Labels
// int input   = 0;	// Bottom [input]
// int output1 = 1;	// right
// int output2 = 2;	// left
// int wall    = 3;	// borders
// int inter   = 4;	// interface

macro Wall()3//
macro OutL()1,3//
macro InL()0//
int[int] Lab = [InL, OutL];

// ====== ====== Output of interest
// ------ Average pressure
real DomainArea = int2d(Th)( 1. );
varf vAvgP(p,q) = int2d(Th)( q/DomainArea );
real[int] OOIapt= vAvgP(0,Qh,tgv=0);
matrix OOIap    = OOIapt;

// ------ Pressure drop
real[int,int] OOIdpt(3,Qh.ndof);

// --- Sect area
real[int] SectArea(3);
SectArea = 1.;
SectArea[0] = int1d(Th,0)( 1. );
SectArea[1] = int1d(Th,1)( 1. );
SectArea[2] = int1d(Th,2)( 1. );

// --- Average pressure
{
	varf ooip0(p,q) = int1d(Th,0)(q/SectArea[0]);
	varf ooip1(p,q) = int1d(Th,1)(q/SectArea[1]); 
	varf ooip2(p,q) = int1d(Th,2)(q/SectArea[2]);
	
	real[int] OOIp0 = ooip0(0,Qh,tgv=0);
	real[int] OOIp1 = ooip1(0,Qh,tgv=0);
	real[int] OOIp2 = ooip2(0,Qh,tgv=0);
	
	for(int q1=0; q1<Qh.ndof; q1++){
		OOIdpt(0,q1) = OOIp0[q1];
		OOIdpt(1,q1) = OOIp1[q1];
		OOIdpt(2,q1) = OOIp2[q1];
	}
}

matrix OOIdp = OOIdpt;
OOIdp.thresholding(1e-30);

// ------ Flow rate
real[int,int] OOIfrt(3,2*Vh.ndof);
int[int] BoundIndex = [0,1,2];

// --- External normal
real[int,int] ExtNormal(BoundIndex.n,2);
for(int q1=0; q1<BoundIndex.n; q1++){
	ExtNormal(q1,0) = int1d(Th,BoundIndex[q1])( N.x/SectArea[q1] );
	ExtNormal(q1,1) = int1d(Th,BoundIndex[q1])( N.y/SectArea[q1] );
}

// --- Variational forms
for(int q1=0; q1<BoundIndex.n; q1++){
	varf ooi(u,v) = int1d(Th,BoundIndex[q1])(v*(pi/2));
	real[int] OOI = ooi(0,Vh,tgv=0);
	for(int q2=0; q2<Vh.ndof; q2++){
		OOIfrt(q1,q2)         = OOI[q2]*ExtNormal(q1,0);
		OOIfrt(q1,q2+Vh.ndof) = OOI[q2]*ExtNormal(q1,1);
	}
}
matrix OOIfr = OOIfrt;
OOIdp.thresholding(1e-30);

// ------ Save
if(!(ooipath=="")){
 	cout << "|" << endl;
	cout << "|        dP mat.: "+ooipath+"dpmat.dat" << endl;
	cout << "| Average P mat.: "+ooipath+"apmat.dat" << endl;
 	cout << "| Flow rate mat.: "+ooipath+"frmat.dat" << endl;
 	ffSaveMatrix(OOIdp,  ooipath+"dpmat.dat");
 	ffSaveMatrix(OOIap,  ooipath+"apmat.dat");
 	ffSaveMatrix(OOIfr, ooipath+"frmat.dat");
}

// ------ End
cout << "|" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;
