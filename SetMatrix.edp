// ------ Header
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|        Matrices setter         |" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;

verbosity = 0;
include "getARGV.idp"
include "./module/wksl.idp"
include "./module/ffmatlib/ffmatlib.idp"

// --- Path
string DecPath = "./decompo/";
string MatPath = "./algebraic/";

// ------ Options
macro TGV()1e30//
bool full  = usedARGV("-full")!=-1;
if(full)  cout << "| # Matrix => full" << endl;
if(!full) cout << "| # Matrix => thresholded" << endl;

// ------ Mesh and FE spaces
mesh Th = readmesh("./mesh/Th.msh");
//fespace Yh(Th,[P2,P2,P1]);
fespace Vh(Th,P2);
fespace Qh(Th,P1);

// ------ Labels
int input   = 0;	// bottom
int output1 = 1;	// right out
int output2 = 2;	// left out
int wall    = 3;	// wall
int inter   = 4;	// interface

// ------ ------ Decompositions
int NbParam = 6;
int nbt = 13;
real Qa = 32;
real Qb = 40;
real Qf = 4;

cout << "| # Decomposition setting : " << endl;
cout << "|   Qa = "+Qa+ " (+2BC)" << endl;
cout << "|   Qb = "+Qb << endl;
cout << "|   Qf = "+Qf << endl;


// ------ Boundary Conditions
matrix tmp, Mtmp;

cout << "| # Set " << endl;
cout << "|  A matrices ... " << endl;
// ------ A
matrix[int] Ah(Qa+2);

// --- Dec.A [line->region | col-> 1=dxdx 2=dydy 3=dxdy]
int[int,int] da = [
	[ 0,  1,  2],
	[ 0,  1,  2],
	[ 3,  4,  5],
	[ 6,  7,  8],
	[ 9, 10, 11],
	[12, 13, 14],
	[15, 16, 17],
	[15, 16, 17],
	[18, 19, 20],
	[21, 22, 23],
	[24, 25, 26],
	[27, 28, 29],
	[27, 28, 29] ];

// --- Boundary conditions
varf BCLv(u,v) = on(wall,u=0);
tmp = BCLv(Vh,Vh,tgv=TGV);
Mtmp= [[tmp,0],[0,tmp]];
Ah[Qa] = Mtmp;

varf BCAv(u,v) = on(input, u=0);
tmp = BCAv(Vh,Vh,tgv=TGV);
Mtmp= [[tmp,0],[0,tmp]];
Ah[Qa+1] = Mtmp;

varf BCv(u,v) = on(wall, input, u=0);
tmp = BCv(Vh,Vh,tgv=TGV);
matrix BC = [[tmp,0],[0,tmp]];

// --- Windkessel matrices
matrix Wksl;
wkslmat(Th,output1,tmp);
Ah[Qa-2] = [[tmp,0],[0,tmp]];

wkslmat(Th,output2,tmp);
Ah[Qa-1] = [[tmp,0],[0,tmp]];

// --- Assembling bilinear forms
for(int q=0; q<Qa; q++){
	for(int reg=0; reg<nbt; reg++){
		for(int theta=0; theta<3; theta++){
			// --- dxdx
			if(theta==0 && da(reg,theta)==q){
				varf Av(u,v) = int2d(Th,reg)( dx(u)*dx(v) ) 
					+ on(wall, input,u=0);
				tmp   = Av(Vh,Vh,tgv=TGV);
				Mtmp  = [[tmp,0],[0,tmp]];
				Ah[q] = Ah[q] + Mtmp - BC;
			}
			// --- dydy
			if(theta==1 && da(reg,theta)==q){
				varf Av(u,v) = int2d(Th,reg)( dy(u)*dy(v) )
					+ on(wall, input,u=0);
				tmp = Av(Vh,Vh,tgv=TGV);
				Mtmp  = [[tmp,0],[0,tmp]];
				Ah[q] = Ah[q] + Mtmp - BC;
			}
			// --- dxdy + dydx
			if(theta==2 && da(reg,theta)==q ){
				varf Av(u,v) = int2d(Th,reg)(dx(u)*dy(v)+dy(u)*dx(v)) 
					+ on(wall, input,u=0);
				tmp = Av(Vh,Vh,tgv=TGV);
				Mtmp  = [[tmp,0],[0,tmp]];
				Ah[q] = Ah[q] + Mtmp - BC;
			}
		}
	}
}


// --- Saving
{ofstream ofl("./algebraic/Ah.dat");
	for(int q=0; q<Qa+2; q++){
		if(!full) Ah[q].thresholding(9e-16);
		ofl << Ah[q] << endl << endl;
	}
}
 
cout << "|  B matrices ... " << endl;
// ------ B
matrix[int] Bh(Qb);
int[int,int] db = [
	[ 0,  1,  2,  3],
	[ 0,  1,  2,  3],
	[ 4,  5,  6,  7],
	[ 8,  9, 10, 11],
	[12, 13, 14, 15],
	[16, 17, 18, 19],
	[20, 21, 22, 23],
	[20, 21, 22, 23],
	[24, 25, 26, 27],
	[28, 29, 30, 31],
	[32, 33, 34, 35],
	[36, 37, 38, 39], 
	[36, 37, 38, 39] ];


// --- Assembling bilinear forms
for(int q=0; q<Qb; q++){
	matrix Mtmp;
	for(int reg=0; reg<nbt; reg++){
		for(int theta=0; theta<4; theta++){
			// --- d1/dx
			if(theta==0 && db(reg,theta)==q){
				varf Bv(u,q) =-int2d(Th,reg)( dx(u)*q );
					//+ on(wall, input,u=0);
				tmp = Bv(Vh,Qh,tgv=TGV);
				matrix O(tmp.n,tmp.m);
				Mtmp  =[[tmp,O]];
				Bh[q] = Bh[q] + Mtmp;
			}
			// --- d2/dy
			if(theta==1 && db(reg,theta)==q){
				varf Bv(u,q) =-int2d(Th,reg)( dy(u)*q );
					//+ on(wall, input,u=0);
				tmp = Bv(Vh,Qh,tgv=TGV);
				matrix O(tmp.n,tmp.m);
				Mtmp  =[[O,tmp]];
				Bh[q] = Bh[q] + Mtmp;
			}
			// --- d2/dx
			if(theta==2 && db(reg,theta)==q ){
				varf Bv(u,q) =-int2d(Th,reg)( dx(u)*q );
					//+ on(wall, input,u=0);
				tmp = Bv(Vh,Qh,tgv=TGV);
				matrix O(tmp.n,tmp.m);
				Mtmp  =[[O,tmp]];
				Bh[q] = Bh[q] + Mtmp;
			}
			// --- d1/dy
			if(theta==3 && db(reg,theta)==q ){
				varf Bv(u,q) =-int2d(Th,reg)( dy(u)*q );
					//+ on(wall, input,u=0);
				tmp = Bv(Vh,Qh,tgv=TGV);
				matrix O(tmp.n,tmp.m);
				Mtmp  =[[tmp,O]];
				Bh[q] = Bh[q] + Mtmp;
			}
		}
	}
}

// --- Saving
{ofstream ofl("./algebraic/Bh.dat");
	ofl.precision(16);
	for(int q=0; q<Qb; q++){
		if(!full) Bh[q].thresholding(9e-16);
		ofl << Bh[q] << endl << endl;
	}
}

// ----- Inner product
cout << "|  Inner products  matrices ... " << endl;
// --- I.P. velocity
varf XhUv(u,v) = int2d(Th)( [dx(u),dy(u)]'*[dx(v),dy(v)] + u*v ) 
	+ on(wall,input,u=0);
matrix XhU = XhUv(Vh,Vh,tgv=TGV);

// --- I.P. pressure
varf XhPv(p,q) = int2d(Th)( p*q );
matrix XhP = XhPv(Qh,Qh,tgv=TGV);

// --- Saving
{ofstream ofl(MatPath+"Xh.dat");
	if(!full){
		XhU.thresholding(1e-30);
		XhP.thresholding(1e-30);
	}
	ofl.precision(16);
	ofl << XhU << endl << endl;
	ofl << XhP << endl << endl;
}

// ------ FE struct
ffSaveVh(Th,Vh,"./algebraic/Vh.dat");
ffSaveVh(Th,Qh,"./algebraic/Qh.dat");

cout << "#===== ====== ====== ====== =====#" << endl;
