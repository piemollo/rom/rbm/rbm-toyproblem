%%%%%% %%%%%% Compute and compare several inf-sup const
addpath ../module/rbf/

 rand('seed',82);
%rng(82);
load('./algebraic_bin.mat');

% --- Parameter space
P = [0.5, 0.8; 0.1, 0.3; (((90-45)/180)*pi), (((90-60)/180)*pi); ...
  (((90-10)/180)*pi), (((90-45)/180)*pi); 0, 500; 0, 1; 0.6, 0.7; ...
  -300, 300];
Pavg = (P(:,2) + 3 .* P(:,1))./4;

%% --- Parameter sample
N = 50;
Pmu = repmat(Pavg',N,1);
rg1 = rand(N,1) .* (P(1,2)-P(1,1)) + P(1,1);
rg2 = rand(N,1) .* (P(5,2)-P(5,1)) + P(5,1);

Pmu(:,1) = rg1;
Pmu(:,5) = rg2;

if(0)
  % --- Get const
  beta = zeros(N,1);

  for n = 1:N
    beta(n) = compute_infsup(mdl, Pmu(n,:));
    fprintf(" %d/%d -+- beta=%f\n", n,N,beta(n));
  end
end

%%
N_train = 10;
N_test  = N - N_train;

beta_train = beta(1:N_train);
beta_test  = beta((1+N_train):end);
P_train    = Pmu(1:N_train,:);
P_test     = Pmu((1+N_train):end,:);

sigma = 6e-5;
phi = rbf_kernel('gaussian', sigma);
gam = rbf_coef(phi, Pmu(1:N_train,:), beta_train, 'lin');
est = rbf_val(phi, gam, P_train, P_test);

save('./rbf10.mat', 'gam', 'sigma', 'P_train', 'beta_train', '-mat');

figure(1)
clf

[betat,I] = sort(beta_test);

plot(betat,'o')
hold on
plot(est(I),'*')

estf = flip(est);
betf = flip(beta_test);
norm(estf(1:10) - betf(1:10))

assert(0);
%% ====== Full random % --- Parameter space
P = [0.5, 0.8; 0.1, 0.3; (((90-45)/180)*pi), (((90-60)/180)*pi); ...
  (((90-10)/180)*pi), (((90-45)/180)*pi); 0, 500; 0, 1; 0.6, 0.7; ...
  -300, 300];

%% --- Parameter sample
N = 50;
Pmu_rand = rand(8,N) .* (P(:,2)-P(:,1)) + P(:,1);
Pmu_rand = Pmu_rand';

if(0)
  % --- Get const
  beta_rand = zeros(N,1);

  for n = 1:N
    beta_rand(n) = compute_infsup(mdl, Pmu_rand(n,:));
    fprintf(" %d/%d -+- beta=%f\n", n, N, beta_rand(n));
  end
end

%%
%est = rbf_val(phi, rbf.gam, rbf.P_train, Pmu_rand);
est = rbf_val(phi, gam, P_train, Pmu_rand);

figure(2)
clf
hold on

[betat, I] = sort(beta_rand);

plot(betat,'o')
plot(est(I),'*')


