function [ca,cb,cf] = mu_decomposition(mu)
  % --- Init
  ca = zeros(32,1);
  cb = zeros(40,1);
  
  % --- Nodes
	rind = [0,0,1,2,3,4,5,5,6,7,8,9,9];
	l = sqrt(2);
  
  % Ref points
  rpt = ...
  [0. , 0.  ; 1. , 0.  ; 1. , 1.  ; 1. , 2.  ; 1.5, 2.5 ;  2. , 3.  ; ...
   1.5, 3.5 ; 1. , 3.  ; 0.5, 2.5 ; 0. , 3.  ;-0.5, 3.5 ; -1. , 3.  ; ...
   -0.5, 2.5 ; 0. , 2.  ; 0. , 1. ];
   FixNod = [1, 2, 3, 4, 14, 15];
  
  % Moved points
  mpt = rpt;
  TMP = zeros(2);
  mpt(6,1) = 1 + l*cos(mu(3)); 
	mpt(6,2) = 2 + l*sin(mu(3));
	
	mpt(7,1) = mpt(6,1) - mu(1)*sin(mu(3)); 
	mpt(7,2) = mpt(6,2) + mu(1)*cos(mu(3));
	
	TMP(1,1) = mpt(7,1) - l*cos(mu(3)); 
	TMP(1,2) = mpt(7,2) - l*sin(mu(3));
	tmp1 =      ( TMP(1,2) - mpt(7,2) )/( TMP(1,1) - mpt(7,1) );
	tmp2 =      ( TMP(1,1)*mpt(7,2) - mpt(7,1)*TMP(1,2) );
	tmp2 = tmp2/( TMP(1,1) - mpt(7,1) );
	mpt(12,1) =   - l*cos(mu(4)); 
	mpt(12,2) = 2 + l*sin(mu(4));
	
	mpt(11,1) = mpt(12,1) + mu(2)*sin(mu(4));
	mpt(11,2) = mpt(12,2) + mu(2)*cos(mu(4));
	
	TMP(2,1) = mpt(11,1) + l*cos(mu(4)); 
	TMP(2,2) = mpt(11,2) - l*sin(mu(4));
	tmp3 =      ( TMP(2,2) - mpt(11,2) )/( TMP(2,1) - mpt(11,1) );
	tmp4 =      ( TMP(2,1)*mpt(11,2) - mpt(11,1)*TMP(2,2) );
	tmp4 = tmp4/( TMP(2,1) - mpt(11,1) );
	mpt(9,1) = (tmp4-tmp2)/(tmp1-tmp3); 
	mpt(9,2) = tmp1*(tmp4-tmp2)/(tmp1-tmp3) + tmp2;
	
	mpt(5,1) = mpt(6,1)*0.5 + 0.5; 
	mpt(5,2) = mpt(6,2)*0.5 + 1;
	
	mpt(13,1) = mpt(12,1)*0.5; 
	mpt(13,2) = mpt(12,2)*0.5 + 1;
	
	mpt(8,2) = TMP(1,2) + mpt(5,2) - 2; 
	mpt(8,1) = (mpt(8,2)-tmp2)/tmp1;
	
	mpt(10,2) = TMP(2,2) + mpt(13,2) - 2; 
	mpt(10,1) = (mpt( 10,2)-tmp4)/tmp3;
  
  % --- Output
  OutLab = [8,13];
  Nout   = [1/l, -1/l; 1/l, 1/l];
  tri = [0, 1,14; 1, 2,14;14, 2, 8; 2, 3, 8; 3, 4, 8; 8, 4, 7; ...
   4, 5, 7; 7, 5, 6;14, 8,13;13, 8,12;12, 8, 9;12, 9,11;11, 9,10];
  tri = tri + 1;
  
  % --- Matrices
  nbt = 13;
  for i=1:nbt
    Ma = UtoD(tri(i,:),mpt)*DtoU(tri(i,:), rpt);
    Md = det(Ma);
    Mb = Ma^-1;
    Ma = Mb*Mb';
    
    ca(3*rind(i)+1) = Ma(1,1)*Md;
    ca(3*rind(i)+2) = Ma(2,2)*Md;
    ca(3*rind(i)+3) = Ma(2,1)*Md;
    
    cb(4*rind(i)+1) = Mb(1,1)*Md;
    cb(4*rind(i)+2) = Mb(2,2)*Md;
    cb(4*rind(i)+3) = Mb(1,2)*Md;
    cb(4*rind(i)+4) = Mb(2,1)*Md;
  end
  
  ca = ca .* mu(7);
  ca(31) = mu(5)*(mu(1)/(0.5*l))^2;
  ca(32) = mu(6)*(mu(2)/(0.5*l))^2;
  
  cf = [ca;cb];
end

% ====== Local functions
% --- Unitary => Deformed
function [A,B] = UtoD(pt, coord)
  A = zeros(2);
  B = zeros(2,1);
  
  % --- A
	A(1,1) = coord(pt(2),1) - coord(pt(1),1);
	A(1,2) = coord(pt(3),1) - coord(pt(1),1);
	A(2,1) = coord(pt(2),2) - coord(pt(1),2);
	A(2,2) = coord(pt(3),2) - coord(pt(1),2);
	% --- B
	B(1,1) = coord(pt(1),1);
	B(2,1) = coord(pt(1),2);
end

% --- Deformed => Unitary
function [A,B] = DtoU(pt, coord)
  A = zeros(2);
  B = zeros(2,1);
  
	% --- det(A) : 1/((x2-x1)*(y3-y1)-(x3-x1)*(y2-y1))
	d =     (coord(pt(2),1)-coord(pt(1),1))*(coord(pt(3),2)-coord(pt(1),2));
	d = d - (coord(pt(3),1)-coord(pt(1),1))*(coord(pt(2),2)-coord(pt(1),2));
	d = 1/d;
	
	% --- A
	A(1,1) =(coord(pt(3),2) - coord(pt(1),2))*d;
	A(1,2) =(coord(pt(1),1) - coord(pt(3),1))*d;
	A(2,1) =(coord(pt(1),2) - coord(pt(2),2))*d;
	A(2,2) =(coord(pt(2),1) - coord(pt(1),1))*d;
	
	% --- B
	B(1,1) =          (coord(pt(3),2) - coord(pt(1),2))*coord(pt(1),1);
	B(1,1) = B(1,1) + (coord(pt(1),1) - coord(pt(3),1))*coord(pt(1),2);
	B(1,1) = B(1,1) * -d;
	
	B(2,1) =          (coord(pt(1),2) - coord(pt(2),2))*coord(pt(1),1);
	B(2,1) = B(2,1) + (coord(pt(2),1) - coord(pt(1),1))*coord(pt(1),2);
	B(2,1) = B(2,1) * -d;
end