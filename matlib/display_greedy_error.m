%%%%%% %%%%%%
load('./results_th.mat');
load('./assets/snap_data.mat')
figpath = './assets/';


%%
figure(1)
clf
set(gcf, 'Renderer', 'painters', 'Position', [0 0 450 300])
grid on

ms = 7;
lw = 1.3;

[err,I] = max(Err,[],1);
[est,J] = max(Est,[],1);
merr = mean(Err./snap.norm,1);
mest = mean(Est./snap.norm,1);
% est = err;
% for i = 1:length(I)
%     est(i) = Est(I(i),i);
% end

semilogy(1:3:(3*size(Err,2)), err,'*:', 'markersize', ms, ...
    'linewidth', lw, 'color', [0 0.4470 0.7410])
hold on
grid on
semilogy(1:3:(3*size(Err,2)), est,'o:', 'markersize', ms, ...
    'linewidth', lw, 'Color',[0.8500 0.3250 0.0980])


legend('True', 'Estimate', 'interpreter', 'latex', 'fontsize', 12)
xlabel('Reduced basis size $N$','interpreter','latex')
ylabel('Max. absolut error','interpreter','latex')
set(gca, 'fontsize', 12)

% saveas(gcf, sprintf('%serror_greedy.png',figpath));
% savefig(gcf, sprintf('%serror_greedy.fig',figpath));

%% 
figure(2)
clf
set(gcf, 'Renderer', 'painters', 'Position', [0 0 450 300])
grid on

ms = 7;
lw = 1.3;

semilogy(1:3:(3*size(Err,2)), merr,'*:', 'markersize', ms, ...
    'linewidth', lw, 'color', [0 0.4470 0.7410])
hold on
grid on
semilogy(1:3:(3*size(Err,2)), mest,'o:', 'markersize', ms, ...
    'linewidth', lw, 'Color',[0.8500 0.3250 0.0980])

legend('True', 'Estimate', 'interpreter', 'latex', 'fontsize', 12)
xlabel('Reduced basis size $N$','interpreter','latex')
ylabel('Mean relative error','interpreter','latex')
set(gca, 'fontsize', 12)

% saveas(gcf, sprintf('%smerror_greedy.png',figpath));
% savefig(gcf, sprintf('%smerror_greedy.fig',figpath));