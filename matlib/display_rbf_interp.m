%%%%%% %%%%%% Compute and compare several inf-sup const
addpath ../module/rbf/
figpath = './assets/';

rng(79);
load('./algebraic_bin.mat');

% --- Parameter space
P = [0.5, 0.8; 0.1, 0.3; (((90-45)/180)*pi), (((90-60)/180)*pi); ...
  (((90-10)/180)*pi), (((90-45)/180)*pi); 0, 500; 0, 1; 0.6, 0.7; ...
  -300, 300];

%% --- Parameter sample
N = 50;
Pmu_rand = rand(8,N) .* (P(:,2)-P(:,1)) + P(:,1);
Pmu_rand = Pmu_rand';

if(1)
  % --- Get const
  beta_rand = zeros(N,1);

  for n = 1:N
    beta_rand(n) = compute_infsup(mdl, Pmu_rand(n,:));
    fprintf(" %d/%d -+- beta=%f\n", n, N, beta_rand(n));
  end
end

%% ====== Estimation
rbf = load('./rbf40.mat');

phi = rbf_kernel('gaussian', rbf.sigma);
est = rbf_val(phi, rbf.gam, rbf.P_train, Pmu_rand);

figure(1)
clf
set(gcf, 'Renderer', 'painters', 'Position', [0 0 450 300])
hold on
grid on

ms = 7;
lw = 1;

[betat, I] = sort(beta_rand);
plot(betat,'o', 'markersize', ms, 'linewidth', lw+0.5)
plot(est(I),'*', 'markersize', ms, 'linewidth', lw)

legend('True','Estimate','interpreter','latex','fontsize',12,'location','northwest')
% xlabel(sprintf('$\\mu_{%d}$',fld-1),'interpreter','latex')
ylabel('$\beta$ constant','interpreter','latex')
set(gca, 'fontsize', 12)

saveas(gcf, sprintf('%sest_beta_40.png',figpath));
savefig(gcf, sprintf('%sest_beta_40.fig',figpath));
