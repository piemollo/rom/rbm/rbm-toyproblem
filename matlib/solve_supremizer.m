function [w] = solve_supremizer(mdl, mu, p)
  
  % --- Coefficients
  [ca,cb] = mu_decomposition(mu);
  
  % --- Build matrices
  B = cb(1)*mdl.Bh{1};
  for q = 2:mdl.Qb
    B = B + cb(q)*mdl.Bh{q};
  end
  
  % --- Inner product
  O = sparse(mdl.NdofU,mdl.NdofU);
  X = [mdl.Xh{1},O;O,mdl.Xh{1}];

  % --- Solve
  w = X\(B'*p);
  w = w.*(abs(w)>1e-16);
end