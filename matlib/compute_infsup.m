function [beta] = compute_infsup(mdl, mu)
  
  % --- Inner product matrix
  Ou = sparse(mdl.NdofU,mdl.NdofU);
  Op = sparse(mdl.NdofP,mdl.NdofU);
  X = [mdl.Xh{1}, Ou, Op'; Ou, mdl.Xh{1}, Op'; Op, Op, mdl.Xh{2}];
  
  % --- Coefficients
  [ca,cb] = mu_decomposition(mu);
  
  % --- Build matrices
  A = ca(1)*mdl.Ah{1};
  for q = 2:mdl.Qa
    A = A + ca(q)*mdl.Ah{q};
  end
  
  B = cb(1)*mdl.Bh{1};
  for q = 2:mdl.Qb
    B = B + cb(q)*mdl.Bh{q};
  end
  
  % --- LFS
  O = sparse(mdl.NdofP,mdl.NdofP);
  M = [A,B';B,O];

  % --- Cnst
  beta = eigs(M,X,1,'lm');
  
end