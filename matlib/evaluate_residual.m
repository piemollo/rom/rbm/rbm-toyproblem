function [residual] = evaluate_residual(res, mu, alpha)
  
  % --- Coefficients
  [ca,cb,cf] = mu_decomposition(mu); 
  cf = cf .* -mu(8);
  cab= [ca;cb];
  a = reshape(alpha,[],1);
  
  % F'XF
  FXF = sum(sum(res.C .* (cf*cf')));
  
  % F'XA
  FXA = 0;
  for q1 = 1:size(res.D,2)
    for q2 = 1:size(res.D,3)
      FXA = FXA + cab(q1)*cf(q2)*(a'*res.D(:,q1,q2));
    end
  end
  
  % A'XA
  AXA = 0;
  for q1 = 1:size(res.E,3)
    for q2 = 1:size(res.E,4)
      AXA = AXA + cab(q1)*cab(q2)*(a'*res.E(:,:,q1,q2)*a);
    end
  end
  
  residual = FXF - 2*FXA + AXA;