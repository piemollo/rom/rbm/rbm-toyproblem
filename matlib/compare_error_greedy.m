%%%%%% %%%%%% Compute and compare several inf-sup const
rand('seed',83);
load('./algebraic_bin.mat');
addpath ../module/ffmatlib/ffmatlib/

% --- Parameter space
P = [0.5, 0.8; 0.1, 0.3; (((90-45)/180)*pi), (((90-60)/180)*pi); ...
  (((90-10)/180)*pi), (((90-45)/180)*pi); 0, 500; 0, 1; 0.6, 0.7; ...
  -300, 300];

% --- Parameter sample
N = 500;
Pmu = rand(8,N) .* (P(:,2)-P(:,1)) + P(:,1);
Pmu = Pmu';

if(0)
  % --- Compute snapshots
  snap.u = zeros(mdl.NdofU*2,N);
  snap.p = zeros(mdl.NdofP,N);
  snap.Pmu = Pmu;
  snap.beta= zeros(N,1);

  for n = 1:N
    [snap.u(:,n), snap.p(:,n)] = solve_FOM(mdl, Pmu(n,:), 'nolift');
    snap.beta(n) = compute_infsup(mdl, Pmu(n,:));
    fprintf(" %d/%d -+- \n", n,N);
  end
  
  save('./assets/snap_data.mat', 'snap', '-mat');
end

% --- Est. inf-sup const
rbf = load('./rbf10.mat');
phi = rbf_kernel('Gaussian',rbf.sigma);
snap.betaest = rbf_val(phi, rbf.gam, rbf.P_train, Pmu);

% --- Greedy algorithm
eps = 1e-1;
Nmax= 20;
[rom, Err, Est, Res] = greedy_algo_true(mdl, Pmu, eps, Nmax, snap);

% --- Figure
figure(1)
hold on 

for i = 1:20
  [err,I] = sort(Err(:,i)./snap.norm(i));
  semilogy(err,'b')
  semilogy(Res(I,20),'r:')
end
%hold on
%semilogy(Est)
%semilogy(Err)


% --- Snap norm
X = mdl.Xh{1};
O = sparse(mdl.NdofU,mdl.NdofU);
X = [X,O;O,X];
O = sparse(mdl.NdofP,mdl.NdofU*2);
XX = [X,O';O,mdl.Xh{2}];

snap.norm = zeros(N,1);
for n = 1:N
    w = [snap.u(:,n); snap.p(:,n)];
    snap.norm(n) = sqrt(w'*XX*w);
end
  
