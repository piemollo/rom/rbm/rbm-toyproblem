function [rom, Err, Est, Res] = greedy_algo_true(mdl, Pmu, eps, Nmax, snap)
  % --- Initiate
  rom.Vn = [];
  rom.Qn = [];
  Err = [];
  Est = [];
  Res = [];
  eta= eps+1;
  N  = 1;
  Imu= 1;
  
  % Inner product
  X = mdl.Xh{1};
  O = sparse(mdl.NdofU,mdl.NdofU);
  X = [X,O;O,X];
  
  O = sparse(mdl.NdofP,mdl.NdofU*2);
  XX = [X,O';O,mdl.Xh{2}];
  
  % Reduced model
  rom.NdofU = mdl.NdofU;
  rom.NdofP = mdl.NdofP;
  rom.Qa    = mdl.Qa;
  rom.Qb    = mdl.Qb;
  rom.Lh    = mdl.Lh;
  
  tic; t=toc;
  while (eta>eps) && (N<=Nmax)
    
    % --- Solve Fom & Supr
    [u,p] = solve_FOM(mdl, Pmu(Imu,:), 'nolift');
    w = solve_supremizer(mdl, Pmu(Imu,:), p);
    
    % --- Orthonorm
    zeta1 = gramschmidt(u, rom.Vn, X);
    zeta2 = gramschmidt(w, [rom.Vn,zeta1], X);
    psi   = gramschmidt(p, rom.Qn, mdl.Xh{2});
    
    % --- Update
    rom.Vn = [rom.Vn, zeta1, zeta2];
    rom.Qn = [rom.Qn, psi];
    [rom.An, rom.Bn, rom.Fn] = galerkin_projection(mdl, rom.Vn, rom.Qn);
    tic;
    rom.residual = set_residual(mdl, rom);
    tttoc = toc;
    
    % --- Estimate error
    err = zeros(size(Pmu,1),3);
    for i = 1:size(Pmu,1)
      [un,pn,alpha] = solve_ROM(rom, Pmu(i,:), 'nolift');
      err(i,1) = evaluate_residual(rom.residual, Pmu(i,:), alpha);
      err(i,2) = err(i,1)/snap.betaest(i);
%      w = [un;pn] - [snap.u(:,i); snap.p(:,i)];
%      err(i,3) = sqrt(w'*XX*w);
    end
%    Res = [Res, err(:,1)];
    Est = [Est, err(:,2)];
%    Err = [Err, err(:,3)];
    
    % --- Iterate
    [eta, Imu] = max(abs(err(:,2)));
    fprintf('# iter %d: Emax=%8.2f (%8.2fs)\n', N, eta, tttoc);
    N = N + 1;
  end
  
end

% ====== Local function
function [w] = gramschmidt(v,Z,X)
  if size(Z,2) == 0
    w = v;
  else
    w = v - Z*(Z'*(X*v));
  end
  
  nrm = sqrt(w'*X*w);
  w   = w./nrm;
end