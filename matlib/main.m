%%%%%% %%%%%% Main
addpath ../module/ffmatlib/ffmatlib/

% --- Load Mesh
Mesh = ffreadmesh('../mesh/Th.msh');

% --- Load model
load('./algebraic_bin.mat');
mu = [ 0.55, 0.22, (((90-55)/180)*pi), (((90-15)/180)*pi), 0, 0, 0.68, 300];

O = sparse(mdl.NdofU,mdl.NdofU);
X = [mdl.Xh{1},O;O,mdl.Xh{1}];

% --- Display opt
plotu = @(u) ffpdeplot(Mesh, 'Vhseq', mdl.Vh, 'XYdata', ...
  abs(u(1:mdl.NdofU))+abs(u((1:mdl.NdofU)+mdl.NdofU)), 'colormap' ,'default');

% --- Check FOM solver
[u,p] = solve_FOM(mdl, mu);

%% figure(1)
%plotu(u);

% --- Build greed
Nmax = 2;
eps  = 1e-2;
Pmu  = [mu;mu.*1.05];
rom = greedy_algo(mdl, Pmu, eps, Nmax);

% --- Check ROM
[un,pn,alpha] = solve_ROM(rom, mu);

%figure(2)
%plotu(un);
%w = abs(un-u);
%w'*X*w

%% --- 
figpath = './assets/'
MU = snap.Pmu(12,:);
[u,p] = solve_FOM(mdl, MU);
Mesh = ffreadmesh('../mesh/T2h.msh');
plotu = @(u) ffpdeplot(Mesh, 'Vhseq', mdl.Vh, 'XYdata', ...
  abs(u(1:mdl.NdofU))+abs(u((1:mdl.NdofU)+mdl.NdofU)), 'colormap' ,'default', 'Colorbar', 'off');

%
figure(1)
clf

set(gcf, 'Position', [100,100,300, 400])
set(gcf, 'Renderer','painters')
plotu(u)

[w,q] = solve_ROM(rom, MU);
axis off

clb=colorbar('location','eastout','colormap','default');
ylabel(clb,'Velocity amplitude',...
    'interpreter','latex','FontSize',12)

saveas(gcf, sprintf('%ssnap_true.png',figpath));
savefig(gcf, sprintf('%ssnap_true.fig',figpath));

%
figure(2)
clf

set(gcf, 'Position', [100,100,300,400])
set(gcf, 'Renderer','painters')
plotu(w)
axis off

clb=colorbar('location','eastout','colormap','default');
ylabel(clb,'Velocity amplitude',...
    'interpreter','latex','FontSize',12)

saveas(gcf, sprintf('%ssnap_rom.png',figpath));
savefig(gcf, sprintf('%ssnap_rom.fig',figpath));

%
figure(3)
clf

set(gcf, 'Position', [100,100,300, 400])
set(gcf, 'Renderer','painters')
plotu(abs(u-w))
axis off

clb=colorbar('location','eastout','colormap','default');
ylabel(clb,'Difference',...
    'interpreter','latex','FontSize',12)

saveas(gcf, sprintf('%ssnap_dif.png',figpath));
savefig(gcf, sprintf('%ssnap_dif.fig',figpath));





tic;
for i = 1:100
  [u,p] = solve_FOM(mdl, mu);
end
toc