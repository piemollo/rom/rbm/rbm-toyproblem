// ====== Header
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|      Build reference mesh      |" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;

// ------ Library
verbosity = 0;
include "getARGV.idp"

// ------ Settings
int nn   =  getARGV("-nn",15);
int disp =  getARGV("-disp",1);

// ------ Labels
int input   = 0;	// Bottom [input]
int output1 = 1;	// right
int output2 = 2;	// left
int wall    = 3;	// borders
int inter   = 4;	// interface

// ------ Function to build triangle
func int triangle(mesh &T, real[int] &p1, real[int] &p2, real[int] p3,
					real[int] lab, int n){
	// --- Borders
	border b1(t=0., 1.){x=(p2[0]-p1[0])*t + p1[0];
			    		y=(p2[1]-p1[1])*t + p1[1];
			 			label=lab[0]; };                     
	border b2(t=0., 1.){x=(p3[0]-p2[0])*t + p2[0];
			    		y=(p3[1]-p2[1])*t + p2[1];
						label=lab[1]; };                     
	border b3(t=0., 1.){x=(p1[0]-p3[0])*t + p3[0];
			    		y=(p1[1]-p3[1])*t + p3[1];
			 			label=lab[2]; };
	
	// --- Lengths
	real l1 = sqrt( (p2[0]-p1[0])^2 + (p2[1]-p1[1])^2 );
	real l2 = sqrt( (p3[0]-p2[0])^2 + (p3[1]-p2[1])^2 );
	real l3 = sqrt( (p1[0]-p3[0])^2 + (p1[1]-p3[1])^2 );
	
	// --- Mesh
	T = buildmesh( b1(l1*n) + b2(l2*n) + b3(l3*n) );
	
	return 0;
}

// ------ ------ Geometry
// ------ Ref points
int nbp = 15;		// Point number
real[int,int] Pt(nbp,2);
Pt( 0,0) = 0.  ; Pt( 0,1) = 0. ;
Pt( 1,0) = 1.  ; Pt( 1,1) = 0. ;
Pt( 2,0) = 1.  ; Pt( 2,1) = 1. ;
Pt( 3,0) = 1.  ; Pt( 3,1) = 2. ;
Pt( 4,0) = 1.5 ; Pt( 4,1) = 2.5;
Pt( 5,0) = 2.  ; Pt( 5,1) = 3. ;
Pt( 6,0) = 1.5 ; Pt( 6,1) = 3.5;
Pt( 7,0) = 1.  ; Pt( 7,1) = 3. ;
Pt( 8,0) = 0.5 ; Pt( 8,1) = 2.5;
Pt( 9,0) = 0.  ; Pt( 9,1) = 3. ;
Pt(10,0) =-0.5 ; Pt(10,1) = 3.5;
Pt(11,0) =-1.  ; Pt(11,1) = 3. ;
Pt(12,0) =-0.5 ; Pt(12,1) = 2.5;
Pt(13,0) = 0.  ; Pt(13,1) = 2. ;
Pt(14,0) = 0.  ; Pt(14,1) = 1. ;

// ------ Ref triangle
int nbt = 13;		// Triangle number
mesh[int] T(nbt);
int[int,int] Tri = [
[  0,  1, 14],
[  1,  2, 14],
[ 14,  2,  8],
[  2,  3,  8],
[  3,  4,  8],
[  8,  4,  7],
[  4,  5,  7],
[  7,  5,  6],
[ 14,  8, 13],
[ 13,  8, 12],
[ 12,  8,  9],
[ 12,  9, 11],
[ 11,  9, 10]
];

// ------ Ref label
int nbl = 4;		// Side label number
real[int,int] Lab = [
[   input,   inter,    wall],
[    wall,   inter,   inter],
[   inter,   inter,   inter],
[    wall,   inter,   inter],
[    wall,   inter,   inter],
[   inter,   inter,    wall],
[    wall,   inter,   inter],
[   inter, output1,    wall],
[   inter,   inter,    wall],
[   inter,   inter,    wall],
[   inter,    wall,   inter],
[   inter,   inter,    wall],
[   inter,    wall, output2]
];


// ------ ------ Construction macro-element 
// ------ Sub-mesh
int nbr = 30;		// Region number
int[int] re;
for(int k=0; k<nbt; k++){
	triangle(T[k], Pt(Tri(k,0),:), Pt(Tri(k,1),:), Pt(Tri(k,2),:), 
		       Lab(k,:), nn);
	if(disp==2) plot(T[k],wait=1,cmm="Sub-mesh "+k);
	re =[0,k];
	T[k] = change(T[k], region=re);
}

// ------ Final mesh 
mesh Th = T[0];
for(int k=1; k<nbt; k++){
	Th = Th + T[k];
}
if(disp==1) plot(Th, wait=1);
savemesh(Th, "./mesh/Th.msh");

// ------ Display
fespace Ch(Th,P0);
Ch H = hTriangle;
real hmin = H[].min;
real hmax = H[].max;
cout << "| Nb elem: "+Th.nt << endl;
cout << "|    hmin: "+hmin  << endl;
cout << "|    hmax: "+hmax  << endl;

// ------ Write RBmsh file
{ofstream ofl("./mesh/Th.RBmsh");
	ofl.precision(16);
	ofl << nbp << " " << nbt << endl;
	ofl << nbr << " " << nbl << endl;
	for(int k=0; k<nbp; k++){
		ofl << Pt(k,0) << " " << Pt(k,1) << endl;
	}
	for(int k=0; k<nbt; k++){
		ofl << Tri(k,0) << " " ;	
		ofl << Tri(k,1) << " " ;	
		ofl << Tri(k,2) << endl;	
	}
}

// // ------ ------ Creat BC matrices
// fespace Yh(Th, [P2,P2,P1]);
// varf bca([u1,u2,p],[v1,v2,q]) = on(input, u1=0., u2=0.);
// varf bcl([u1,u2,p],[v1,v2,q]) = on(wall , u1=0., u2=0.);
// varf bce([u1,u2,p],[v1,v2,q]) =//  on( output, u1=1.) +
// 								on( wall, input, u1=1., u2=1.);
// matrix BCA = bca(Yh,Yh);
// matrix BCL = bcl(Yh,Yh);
// matrix BCE = bce(Yh,Yh,tgv=1);  // B.C. in sym.elem. (for ev)
// 
// {ofstream ofl("./algebraic/BCmatrix.dat");
// 	ofl.precision(16);
// 	ofl << BCA << endl << endl;
// 	ofl << BCL << endl << endl;
// }
// // --- Save mat
// {ofstream ofl("./algebraic/BCev.dat");
// 	ofl.precision(16);
// 	BCE.thresholding(1e-30);
// 	//BCE = 1e-30*BCE;
// 	ofl << BCE << endl;
// }
// 
// // ------ Save Dirichlet labels
// {ofstream ofl("./algebraic/DirichletBClab.dat");
// 	ofl << 2 << endl;		// Nb of labels
// 	ofl << input << endl;	// List of lab
// 	ofl << wall << endl;	//  
// }

cout << "#===== ====== ====== ====== =====#" << endl;
