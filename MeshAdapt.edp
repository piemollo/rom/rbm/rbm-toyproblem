// ------ Header
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|          Mesh adaptation       |" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;
cout << "|" << endl;

// ------ Parameters
verbosity = 0;
include "getARGV.idp"
real tic, toc; 	// ... on the clock
tic = clock();

// ------ Setting
real nu = 4.02*1e-3;
func fin = x*(1-x)*2.;
mesh Th  = readmesh("./mesh/Th.msh");

// ------ Option
int disp  = getARGV("-disp",0);
int Imax = getARGV("-Imax",10);
real cmin= getARGV("-cmin",1.0);
real cmax= getARGV("-cmax",1.0);
int meshsave = usedARGV("-save")!=-1;

// ------ Labels
int input   = 0;	// Bottom [input]
int output1 = 1;	// right
int output2 = 2;	// left
int wall    = 3;	// borders
int inter   = 4;	// interface

// ------ Adapt loop
for(int I=0; I<Imax; I++){
	// ------ FE spaces
	fespace Xh(Th, P2);
	fespace Qh(Th, P1);
	fespace Ch(Th, P0);
	
	// ------ Variational form
	Xh u1, u2, v1, v2;
	Qh p, q;
	solve SSTKd([u1, u2, p],[v1, v2, q])=
	int2d(Th)(nu*(
		 dx(u1)*dx(v1) + dx(u2)*dx(v2)
		+dy(u1)*dy(v1) + dy(u2)*dy(v2) )
		- p * (dx(v1)+dy(v2))
		- q * (dx(u1)+dy(u2))          )
	+on(input, u1=0., u2=fin)
	+on(wall, u1=0., u2=0.);
	
	// ------ Display
	if(disp>=2 && I==(Imax-1)){
		plot(u1, fill=1, wait=1, value=1, cmm="Velocity ["+I+"]");
		plot(u2, fill=1, wait=1, value=1, cmm="Velocity ["+I+"]");
		plot( p, fill=1, wait=1, value=1, cmm="Pressure ["+I+"]");
	}
	
	// ------ Mesh data
	Ch h = hTriangle;
	real hmin = h[].min;
	real hmax = h[].max;
	
	// ------ Display
	cout << "|  # Iteration ["+I+"]" << endl;
	cout << "|      Nb elem : "+Th.nt << endl;
	cout << "|         hmin : "+hmin << endl;
	cout << "|         hmax : "+hmax << endl;
	cout << "|" << endl;
	
	// ------ Adapt
	mesh ATh = adaptmesh(Th, [u1,u2], hmin=hmin*cmin, hmax=hmax*cmax,
				iso=true);
	if(disp>=1) plot(ATh, cmm="Mesh ["+I+"]");
	Th = ATh;
}

// ------ Optional save
if(meshsave){
	savemesh(Th,"./mesh/Th.msh");
	
	// // ------ Update BC matrices
	// fespace Yh(Th, [P2,P2,P1]);
	// varf bca([u1,u2,p],[v1,v2,q]) = on(input, u1=0., u2=0.);
	// varf bcl([u1,u2,p],[v1,v2,q]) = on(wall , u1=0., u2=0.);
	// varf bce([u1,u2,p],[v1,v2,q]) =//  on( output, u1=1.) +
	// 								on( wall, input, u1=1., u2=1.);
	// matrix BCA = bca(Yh,Yh);
	// matrix BCL = bcl(Yh,Yh);
	// matrix BCE = bce(Yh,Yh,tgv=1);  // B.C. in sym.elem. (for ev)
	// 
	// {ofstream ofl("./algebraic/BCmatrix.dat");
	// 	ofl.precision(16);
	// 	ofl << BCA << endl << endl;
	// 	ofl << BCL << endl << endl;
	// }
	// 
	// // --- Save mat
	// {ofstream ofl("./algebraic/BCev.dat");
	// 	ofl.precision(16);
	// 	BCE.thresholding(1e-30);
	// 	//BCE = 1e-30*BCE;
	// 	ofl << BCE << endl;
	// }
}

cout << "|                                " << endl;
toc = clock() - tic;
cout << "| Exec time : "+toc+"(s)" << endl;
cout << "#===== ====== ====== ====== =====#" << endl;
