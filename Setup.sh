#!/bin/bash

# --- Set path
# ffpath=.../
export ffpath="$HOME/Prog/FreeFem-sources/install/bin"
export off=" -nw -ne -v 0 -glut $ffpath/ffglut "
export xff="$ffpath/FreeFem++ $off "

# --- Make dir
mkdir -p mesh results algebraic

export nu="0.69";

$xff MeshGen.edp -nn 20 -disp 0 ;
$xff MeshAdapt.edp -cmin 0.5 -cmax 0.8 -Imax 10 -disp 0 -save ;
$xff MeshData.edp -mesh ./mesh/Th.msh -out ./assets/ -disp ;

$xff SetMatrix.edp ;
$xff SetAlgebraic.edp -out_ooi ./algebraic/ ;
$xff LiftingFunction.edp -nu ${nu} ;
